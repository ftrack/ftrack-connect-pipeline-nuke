..
    :copyright: Copyright (c) 2022 ftrack

.. _release/release_notes:

*************
Release Notes
*************

.. release:: upcoming


    .. change:: fixed

        Nuke log viewer and documentation not working


    .. change:: fixed

        Prevented multiple docked publishers and asset managers


    .. change:: fixed

        Frame range not updating on launch


    .. change:: fixed

        Remove event hub disconnect, aligned menu and reordered asset manager engine functions


.. release:: 1.0.1
    :date: 2022-08-01

    .. change:: new

        Initial release

